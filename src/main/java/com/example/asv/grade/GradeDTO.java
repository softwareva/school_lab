package com.example.asv.grade;

import lombok.Data;

@Data
// DTO для уровня школ
public class GradeDTO {
    private Integer id;
    private Integer grade;
}
