package com.example.asv.teacher;

import lombok.Data;

import java.util.List;

@Data
public class TeacherResponseDTO {
    private Integer totalItems;
    private Integer currentPage;
    private List<TeacherDTO> teacherDTOList;
}
