package com.example.asv.teacher;

import com.example.asv.exceptions.CustomApiException;
import com.example.asv.jooq.Tables;
import com.example.asv.jooq.tables.pojos.Teacher;
import com.example.asv.schoolclass.SchoolClassDTO;
import com.example.asv.schoolclass.SchoolClassRepository;
import com.example.asv.student.StudentDTO;
import org.apache.log4j.Logger;
import org.jooq.Condition;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Objects;

import static org.jooq.impl.DSL.trueCondition;

@Service
public class TeacherService {
    static Logger logger = Logger.getLogger(TeacherController.class);

    private final TeacherMapper teacherMapper = new TeacherMapper();
    private final Integer defaultPageSize = 50;
    @Autowired
    public TeacherRepository teacherRepository;
    @Autowired
    public SchoolClassRepository schoolClassRepository;

    public TeacherDTO create (Teacher teacher) throws CustomApiException {
        // Если передали id, то возвращаем ошибку
        if (teacher.getId() != null) {
            logger.error("Id field is prohibited");
            throw new CustomApiException("Id field is prohibited", HttpStatus.BAD_REQUEST);
        }
        // Если не передали fullName, то возвращаем ошибку
        if (teacher.getFullName() == null) {
            logger.error("FullName can't be empty");
            throw new CustomApiException("FullName can't be empty", HttpStatus.BAD_REQUEST);
        }
        // Если не передали fullName, то возвращаем ошибку
        if (Objects.equals(teacher.getFullName(), "")) {
            logger.error("FullName can't be empty");
            throw new CustomApiException("FullName can't be empty", HttpStatus.BAD_REQUEST);
        }
        return teacherRepository.insert(teacher);
    }

    public TeacherDTO getTeacher (Integer id) throws CustomApiException {
        TeacherDTO teacherDTO = teacherRepository.find(id);
        if (teacherDTO == null) {
            logger.error(String.format("Teacher with id %d not found", id));
            throw new CustomApiException(String.format("Teacher with id %d not found", id), HttpStatus.NOT_FOUND);
        }
        return teacherDTO;
    }

    public TeacherResponseDTO getTeachers (TeacherDTO teacherDTO, Integer pageSize, Integer pageNumber) {
        if (pageSize > defaultPageSize) {
            pageSize = defaultPageSize;
        }

        Condition condition = trueCondition();
        if (teacherDTO.getFullName() != null) {
            condition = condition.and(Tables.TEACHER.FULL_NAME.containsIgnoreCase(teacherDTO.getFullName()));
        }

        return teacherMapper.mapToTeacherResponseDTO(
                teacherRepository.findAll(condition, pageSize, pageNumber),
                pageNumber,
                teacherRepository.getTotalItems(condition)
        );
    }

    public boolean delete (Integer id) throws CustomApiException {
        List<SchoolClassDTO> schoolClassDTOList = schoolClassRepository.findSchoolClassesByTeacherId(id);

        // Для этого учителя назначен школьный класс
        if (!schoolClassDTOList.isEmpty()) {
            logger.error(String.format("Some school classes are attached to teacher with id %d", id));
            throw new CustomApiException(String.format("Some school classes are attached to teacher with id %d", id), HttpStatus.BAD_REQUEST);
        }

        TeacherDTO teacherDTO = teacherRepository.find(id);
        if (teacherDTO == null) {
            logger.error(String.format("Teacher with id %d not found", id));
            throw new CustomApiException(String.format("Teacher with id %d not found", id), HttpStatus.NOT_FOUND);
        }
        return teacherRepository.delete(id);
    }

    public TeacherDTO update(Integer id, Teacher teacher) throws CustomApiException {
        TeacherDTO teacherDTO = teacherRepository.update(id, teacher);
        if (teacherDTO == null) {
            logger.error(String.format("Teacher with id %d not found", id));
            throw new CustomApiException(String.format("Teacher with id %d not found", id), HttpStatus.NOT_FOUND);
        }
        return teacherDTO;
    }
}
