package com.example.asv.teacher;

import com.example.asv.exceptions.CustomApiException;
import com.example.asv.jooq.tables.pojos.Teacher;
import com.example.asv.student.StudentController;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping(value = "/teacher")
public class TeacherController {
    static Logger logger = Logger.getLogger(TeacherController.class);

    @Autowired
    private TeacherService teacherService;

    @GetMapping("/")
    public ResponseEntity<TeacherResponseDTO> getTeachers(@RequestParam(required = false) String fullName,
                                                          @RequestParam(required = false, defaultValue = "50") Integer pageSize,
                                                          @RequestParam(required = false, defaultValue = "0") Integer pageNumber) {
        TeacherDTO teacherDTO = new TeacherDTO();
        teacherDTO.setFullName(fullName);

        TeacherResponseDTO teacherResponseDTO = teacherService.getTeachers(teacherDTO, pageSize, pageNumber);
        logger.info("Teacher list sent successfully");
        return ResponseEntity.ok(teacherResponseDTO);
    }

    @GetMapping("/{teacherId}")
    public ResponseEntity<TeacherDTO> getTeacher(@PathVariable(value = "teacherId") Integer id) throws CustomApiException {
        TeacherDTO teacherDTO = teacherService.getTeacher(id);
        logger.info("Teacher sent successfully");
        return ResponseEntity.ok(teacherDTO);
    }

    @PostMapping("/")
    public ResponseEntity<TeacherDTO> createTeacher(@RequestBody Teacher teacher) throws CustomApiException {
        TeacherDTO teacherDTO = teacherService.create(teacher);
        logger.info("Teacher created successfully");
        return ResponseEntity.ok(teacherDTO);
    }

    @PutMapping("/{teacherId}")
    public ResponseEntity<TeacherDTO> updateTeacher(@PathVariable(value = "teacherId") Integer id,
                                                    @RequestBody Teacher teacher) throws CustomApiException {
        TeacherDTO teacherDTO = teacherService.update(id, teacher);
        logger.info("Teacher updated successfully");
        return ResponseEntity.ok(teacherDTO);
    }

    @DeleteMapping("/{teacherId}")
    public ResponseEntity<Object> deleteTeacher(@PathVariable(value = "teacherId") Integer id) throws CustomApiException {
        boolean t = teacherService.delete(id);
        if (t) {
            logger.info("Teacher deleted successfully");
            return ResponseEntity.ok("Teacher deleted");
        }
        else {
            logger.error("Error while deleting teacher");
            return ResponseEntity.badRequest().body("Error");
        }
    }
}
