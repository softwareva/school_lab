package com.example.asv.teacher;

import com.example.asv.jooq.Tables;
import com.example.asv.jooq.tables.pojos.Teacher;
import lombok.RequiredArgsConstructor;
import org.jooq.Condition;
import org.jooq.DSLContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
@RequiredArgsConstructor
public class TeacherRepository {
    @Autowired
    private final DSLContext dslContext;

    public TeacherDTO find (Integer id) {
        return dslContext.selectFrom(Tables.TEACHER)
                .where(Tables.TEACHER.ID.eq(id))
                .fetchOneInto(TeacherDTO.class);
    }

    public List<TeacherDTO> findAll (Condition condition, Integer pageSize, Integer pageNumber) {
        return dslContext.selectFrom(Tables.TEACHER)
                .where(condition)
                .orderBy(Tables.TEACHER.ID)
                .limit(pageSize)
                .offset(pageSize * pageNumber)
                .fetchInto(TeacherDTO.class);
    }

    public TeacherDTO insert(Teacher teacher) {
        return dslContext.insertInto(Tables.TEACHER)
                .set(dslContext.newRecord(Tables.TEACHER, teacher))
                .returning()
                .fetchOne().into(TeacherDTO.class);
    }

    public TeacherDTO update (Integer id, Teacher teacher) {
        return dslContext.update(Tables.TEACHER)
                .set(dslContext.newRecord(Tables.TEACHER, teacher))
                .where(Tables.TEACHER.ID.eq(id))
                .returning()
                .fetchOne().into(TeacherDTO.class);
    }

    public boolean delete (Integer id) {
        return dslContext.delete(Tables.TEACHER)
                .where(Tables.TEACHER.ID.eq(id))
                .execute() == 1;
    }

    public Integer getTotalItems(Condition condition) {
        return dslContext.selectCount()
                .from(Tables.TEACHER)
                .where(condition)
                .fetchOneInto(Integer.class);
    }
}
