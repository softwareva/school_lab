package com.example.asv.teacher;

import lombok.Data;

@Data
public class TeacherDTO {
    private Integer id;
    private String fullName;
}
