package com.example.asv.teacher;

import com.example.asv.jooq.tables.pojos.Teacher;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class TeacherMapper {
    public TeacherDTO mapToTeacherDTO(Teacher teacher) {
        TeacherDTO teacherDTO = new TeacherDTO();
        teacherDTO.setId(teacher.getId());
        teacherDTO.setFullName(teacher.getFullName());

        return teacherDTO;
    }

    public TeacherResponseDTO mapToTeacherResponseDTO(List<TeacherDTO> teacherDTOList, Integer currentPage, Integer totalItems) {
        TeacherResponseDTO teacherResponseDTO = new TeacherResponseDTO();
        teacherResponseDTO.setTeacherDTOList(teacherDTOList);
        teacherResponseDTO.setCurrentPage(currentPage);
        teacherResponseDTO.setTotalItems(totalItems);

        return teacherResponseDTO;
    }
}
