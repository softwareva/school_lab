package com.example.asv.parallel;

import lombok.Data;

@Data
// DTO для параллелей
public class ParallelDTO {
    private Integer id;
    private Integer parallel;
}
