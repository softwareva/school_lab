package com.example.asv.schoolclass;

import com.example.asv.exceptions.CustomApiException;
import com.example.asv.jooq.Tables;
import com.example.asv.jooq.tables.pojos.SchoolClass;
import com.example.asv.student.StudentDTO;
import com.example.asv.student.StudentRepository;
import org.apache.log4j.Logger;
import org.jooq.Condition;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import java.util.List;

import static org.jooq.impl.DSL.trueCondition;

@Service
public class SchoolClassService {
    static Logger logger = Logger.getLogger(SchoolClassService.class);

    private final SchoolClassMapper schoolClassMapper = new SchoolClassMapper();
    private final Integer defaultPageSize = 50;
    @Autowired
    public SchoolClassRepository schoolClassRepository;
    @Autowired
    public StudentRepository studentRepository;

    public SchoolClassDTO create (SchoolClass schoolClass) throws CustomApiException {

        // Если передали id, то возвращаем ошибку
        if (schoolClass.getId() != null) {
            logger.error("Id field is prohibited");
            throw new CustomApiException("Id field is prohibited", HttpStatus.BAD_REQUEST);
        }
        // В запросе не передан id параллели
        if (schoolClass.getIdParallel() == null) {
            logger.error("Missing required field: idParallel");
            throw new CustomApiException("Missing required field: idParallel", HttpStatus.BAD_REQUEST);
        }
        // В запросе не передан id учителя
        if (schoolClass.getIdTeacher() == null) {
            logger.error("Missing required field: idTeacher");
            throw new CustomApiException("Missing required field: idTeacher", HttpStatus.BAD_REQUEST);
        }
        // В запросе не передан год
        if (schoolClass.getCurrentYear() == null) {
            logger.error("Missing required field: currentYear");
            throw new CustomApiException("Missing required field: currentYear", HttpStatus.BAD_REQUEST);
        }

        return schoolClassRepository.insert(schoolClass);
    }

    public SchoolClassDTO getSchoolClass (Integer id) throws CustomApiException {
        SchoolClassDTO schoolClassDTO = schoolClassRepository.find(id);

        if (schoolClassDTO == null) {
            logger.error(String.format("SchoolClass with id %d not found", id));
            throw new CustomApiException(String.format("SchoolClass with id %d not found", id), HttpStatus.NOT_FOUND);
        }
        return schoolClassDTO;
    }

    public SchoolClassResponseDTO getSchoolClasses (SchoolClassDTO schoolClassDTO, Integer pageSize, Integer pageNumber) {
        if (pageSize > defaultPageSize) {
            pageSize = defaultPageSize;
        }

        Condition condition = trueCondition();
        if (schoolClassDTO.getCurrentYear() != null) {
            condition = condition.and(Tables.SCHOOL_CLASS.CURRENT_YEAR.containsIgnoreCase(schoolClassDTO.getCurrentYear()));
        }
        if (schoolClassDTO.getIdTeacher() != null) {
            condition = condition.and(Tables.SCHOOL_CLASS.ID_TEACHER.containsIgnoreCase(schoolClassDTO.getIdTeacher()));
        }
        if (schoolClassDTO.getIdParallel() != null) {
            condition = condition.and(Tables.SCHOOL_CLASS.ID_PARALLEL.containsIgnoreCase(schoolClassDTO.getIdParallel()));
        }

        return schoolClassMapper.mapToSchoolClassResponseDTO(
                schoolClassRepository.findAll(condition, pageSize, pageNumber),
                pageNumber,
                schoolClassRepository.getTotalItems(condition)
        );
    }

    public boolean delete (Integer id) throws CustomApiException {
        List<StudentDTO> studentDTOList = studentRepository.findStudentsBySchoolClassId(id);

        // В этом классе есть ученики
        if (!studentDTOList.isEmpty()) {
            logger.error(String.format("Some students are attached to school class with id %d", id));
            throw new CustomApiException(String.format("Some students are attached to school class with id %d", id), HttpStatus.BAD_REQUEST);
        }

        SchoolClassDTO schoolClassDTO = schoolClassRepository.find(id);
        if (schoolClassDTO == null) {
            logger.error(String.format("SchoolClass with id %d not found", id));
            throw new CustomApiException(String.format("SchoolClass with id %d not found", id), HttpStatus.NOT_FOUND);
        }
        return schoolClassRepository.delete(id);
    }

    public SchoolClassDTO update (Integer id, SchoolClass schoolClass) throws CustomApiException {
        SchoolClassDTO schoolClassDTO = schoolClassRepository.update(id, schoolClass);
        if (schoolClassDTO == null) {
            logger.error(String.format("SchoolClass with id %d not found", id));
            throw new CustomApiException(String.format("SchoolClass with id %d not found", id), HttpStatus.NOT_FOUND);
        }
        return schoolClassDTO;
    }
}
