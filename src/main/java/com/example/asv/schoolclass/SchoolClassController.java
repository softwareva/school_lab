package com.example.asv.schoolclass;

import com.example.asv.exceptions.CustomApiException;
import com.example.asv.jooq.tables.pojos.SchoolClass;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping(value = "/schoolClass")
public class SchoolClassController {
    static Logger logger = Logger.getLogger(SchoolClassController.class);

    @Autowired
    private SchoolClassService schoolClassService;

    @GetMapping("/")
    public ResponseEntity<SchoolClassResponseDTO> getSchoolClasses(@RequestParam(required = false) Integer idParallel,
                                                                   @RequestParam(required = false) Integer idTeacher,
                                                                   @RequestParam(required = false) Integer currentYear,
                                                                   @RequestParam(required = false, defaultValue = "50") Integer pageSize,
                                                                   @RequestParam(required = false, defaultValue = "0") Integer pageNumber) {
        SchoolClassDTO schoolClassDTO = new SchoolClassDTO();
        schoolClassDTO.setIdParallel(idParallel);
        schoolClassDTO.setIdTeacher(idTeacher);
        schoolClassDTO.setCurrentYear(currentYear);

        SchoolClassResponseDTO schoolClassResponseDTO = schoolClassService.getSchoolClasses(schoolClassDTO, pageSize, pageNumber);
        logger.info("School classes list sent successfully");
        return ResponseEntity.ok(schoolClassResponseDTO);
    }

    @GetMapping("/{schoolClassId}")
    public ResponseEntity<SchoolClassDTO> getSchoolClass(@PathVariable(value = "schoolClassId") Integer id) throws CustomApiException {
        SchoolClassDTO schoolClassDTO = schoolClassService.getSchoolClass(id);
        logger.info("School class sent successfully");
        return ResponseEntity.ok(schoolClassDTO);
    }

    @PostMapping("/")
    public ResponseEntity<SchoolClassDTO> createSchoolClass(@RequestBody SchoolClass schoolClass) throws CustomApiException {
        SchoolClassDTO schoolClassDTO = schoolClassService.create(schoolClass);
        logger.info("School class created successfully");
        return ResponseEntity.ok(schoolClassDTO);
    }

    @PutMapping("/{schoolClassId}")
    public ResponseEntity<SchoolClassDTO> updateSchoolClass(@PathVariable(value = "schoolClassId") Integer id,
                                                    @RequestBody SchoolClass schoolClass) throws CustomApiException {
        SchoolClassDTO schoolClassDTO = schoolClassService.update(id, schoolClass);
        logger.info("School class updated successfully");
        return ResponseEntity.ok(schoolClassDTO);
    }

    @DeleteMapping("/{schoolClassId}")
    public ResponseEntity<Object> deleteSchoolClass(@PathVariable(value = "schoolClassId") Integer id) throws CustomApiException {
        boolean t = schoolClassService.delete(id);
        if (t) {
            logger.info("School class deleted successfully");
            return ResponseEntity.ok("SchoolClass deleted");
        }
        else {
            logger.info("Error while deleting school class");
            return ResponseEntity.badRequest().body("Error");
        }
    }
}
