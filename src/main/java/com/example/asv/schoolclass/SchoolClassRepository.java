package com.example.asv.schoolclass;

import com.example.asv.jooq.Tables;
import com.example.asv.jooq.tables.pojos.SchoolClass;
import com.example.asv.jooq.tables.records.SchoolClassRecord;
import com.example.asv.student.StudentDTO;
import lombok.RequiredArgsConstructor;
import org.jooq.Condition;
import org.jooq.DSLContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
@RequiredArgsConstructor
public class SchoolClassRepository {
    @Autowired
    private final DSLContext dslContext;

    public SchoolClassDTO find (Integer id) {
        return dslContext.selectFrom(Tables.SCHOOL_CLASS)
                .where(Tables.SCHOOL_CLASS.ID.eq(id))
                .fetchOneInto(SchoolClassDTO.class);
    }

    public List<SchoolClassDTO> findAll (Condition condition, Integer pageSize, Integer pageNumber) {
        return dslContext.selectFrom(Tables.SCHOOL_CLASS)
                .where(condition)
                .orderBy(Tables.SCHOOL_CLASS.ID)
                .limit(pageSize)
                .offset(pageSize * pageNumber)
                .fetchInto(SchoolClassDTO.class);
    }

    public SchoolClassDTO insert(SchoolClass schoolClass) {
        return dslContext.insertInto(Tables.SCHOOL_CLASS)
                .set(dslContext.newRecord(Tables.SCHOOL_CLASS, schoolClass))
                .returning()
                .fetchOne().into(SchoolClassDTO.class);
    }

    public int[] insertAll(List<SchoolClassRecord> schoolClassRecordList) {
        return dslContext.batchInsert(schoolClassRecordList).execute();
    }

    public SchoolClassDTO update (Integer id, SchoolClass schoolClass) {
        return dslContext.update(Tables.SCHOOL_CLASS)
                .set(dslContext.newRecord(Tables.SCHOOL_CLASS, schoolClass))
                .where(Tables.SCHOOL_CLASS.ID.eq(id))
                .returning()
                .fetchOne().into(SchoolClassDTO.class);
    }

    public boolean delete (Integer id) {
        return dslContext.delete(Tables.SCHOOL_CLASS)
                .where(Tables.SCHOOL_CLASS.ID.eq(id))
                .execute() == 1;
    }

    public List<SchoolClassDTO> findSchoolClassesByTeacherId (Integer id) {
        return dslContext.selectFrom(Tables.SCHOOL_CLASS)
                .where(Tables.SCHOOL_CLASS.ID_TEACHER.eq(id))
                .fetchInto(SchoolClassDTO.class);
    }

    public Integer getTotalItems(Condition condition) {
        return dslContext.selectCount()
                .from(Tables.SCHOOL_CLASS)
                .where(condition)
                .fetchOneInto(Integer.class);
    }
}
