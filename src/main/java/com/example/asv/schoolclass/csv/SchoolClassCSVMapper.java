package com.example.asv.schoolclass.csv;

import com.example.asv.jooq.tables.records.SchoolClassRecord;
import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVParser;
import org.apache.commons.csv.CSVRecord;
import org.springframework.web.multipart.MultipartFile;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.List;

public class SchoolClassCSVMapper {
    public static String TYPE = "text/csv";
    static String[] HEADERs = { "id_parallel", "id_teacher", "current_year" };

    // метод для проверки файла на CSV формат
    public static boolean hasCSVFormat(MultipartFile file) {
        if (!TYPE.equals(file.getContentType())) {
            return false;
        }
        return true;
    }

    // метод для перевода из csv в список школьных классов
    public static List<SchoolClassRecord> csvToSchoolClasses(InputStream is) {
        try (BufferedReader fileReader = new BufferedReader(new InputStreamReader(is, StandardCharsets.UTF_8));
             CSVParser csvParser = new CSVParser(fileReader,
                     CSVFormat.DEFAULT.builder().setHeader().setSkipHeaderRecord(true).setIgnoreHeaderCase(true).setTrim(true)
                             .setDelimiter(';')
                             .setRecordSeparator("\r\n").build())) {

            List<SchoolClassRecord> schoolClassRecordList = new ArrayList<SchoolClassRecord>();

            Iterable<CSVRecord> csvRecords = csvParser.getRecords();

            for (CSVRecord csvRecord : csvRecords) {
                SchoolClassRecord schoolClassRecord = new SchoolClassRecord();
                System.out.println(csvRecord);
                schoolClassRecord.setIdParallel(Integer.parseInt(csvRecord.get("id_parallel")));
                schoolClassRecord.setIdTeacher(Integer.parseInt(csvRecord.get("id_teacher")));
                schoolClassRecord.setCurrentYear(Integer.parseInt(csvRecord.get("current_year")));

                schoolClassRecordList.add(schoolClassRecord);
            }

            return schoolClassRecordList;
        } catch (IOException e) {
            throw new RuntimeException("fail to parse CSV file: " + e.getMessage());
        }
    }
}
