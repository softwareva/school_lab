package com.example.asv.schoolclass.csv;

import com.example.asv.jooq.tables.records.SchoolClassRecord;
import com.example.asv.schoolclass.SchoolClassRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.List;

@Service
public class SchoolClassCSVService {
    @Autowired
    SchoolClassRepository schoolClassRepository;

    // сохранить данные в БД из CSV-файла
    public void save(MultipartFile file) {
        try {
            List<SchoolClassRecord> schoolClassRecordList = SchoolClassCSVMapper.csvToSchoolClasses(file.getInputStream());
            schoolClassRepository.insertAll(schoolClassRecordList);
        } catch (IOException e) {
            throw new RuntimeException("fail to store csv " + e.getMessage());
        }
    }
}
