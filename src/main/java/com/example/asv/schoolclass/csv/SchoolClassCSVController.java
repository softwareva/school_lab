package com.example.asv.schoolclass.csv;

import com.example.asv.schoolclass.SchoolClassController;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

@RestController
@RequestMapping(value = "/schoolClass/csv")
// контроллер для загрузки школьных классов из csv файла
public class SchoolClassCSVController {
    static Logger logger = Logger.getLogger(SchoolClassCSVController.class);

    @Autowired
    SchoolClassCSVService csvService;

    @PostMapping("/upload")
    public ResponseEntity<SchoolClassCSVResponseDTO> uploadFile(@RequestParam("file") MultipartFile file) {
        String message = "";

        // проверяем на формат CSV файл
        if (SchoolClassCSVMapper.hasCSVFormat(file)) {
            try {
                csvService.save(file);

                message = "Uploaded the file successfully: " + file.getOriginalFilename();
                SchoolClassCSVResponseDTO schoolClassCSVResponseDTO = new SchoolClassCSVResponseDTO();
                schoolClassCSVResponseDTO.setMessage(message);

                logger.info(message);

                return ResponseEntity.ok(schoolClassCSVResponseDTO);
            } catch (Exception e) {

                message = "Could not upload the file: " + file.getOriginalFilename() + "!";

                SchoolClassCSVResponseDTO schoolClassCSVResponseDTO = new SchoolClassCSVResponseDTO();
                schoolClassCSVResponseDTO.setMessage(message);
                e.printStackTrace();

                logger.error(message);

                return ResponseEntity.status(HttpStatus.EXPECTATION_FAILED).body(schoolClassCSVResponseDTO);
            }
        }

        message = "Please upload a csv file!";
        SchoolClassCSVResponseDTO schoolClassCSVResponseDTO = new SchoolClassCSVResponseDTO();
        schoolClassCSVResponseDTO.setMessage(message);
        logger.error(message);

        return ResponseEntity.badRequest().body(schoolClassCSVResponseDTO);
    }
}
