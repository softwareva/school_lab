package com.example.asv.schoolclass.csv;

import lombok.Data;

@Data
public class SchoolClassCSVResponseDTO {
    private String message;
}
