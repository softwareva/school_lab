package com.example.asv.schoolclass;

import lombok.Data;

@Data
public class SchoolClassDTO {
    private Integer id;
    private Integer idParallel;
    private Integer idTeacher;
    private Integer currentYear;
}
