package com.example.asv.schoolclass;
;
import lombok.Data;

import java.util.List;

@Data
public class SchoolClassResponseDTO {
    private Integer totalItems;
    private Integer currentPage;
    private List<SchoolClassDTO> schoolClassDTOList;
}
