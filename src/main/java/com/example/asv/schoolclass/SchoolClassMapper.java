package com.example.asv.schoolclass;

import com.example.asv.jooq.tables.pojos.SchoolClass;
import com.example.asv.jooq.tables.pojos.Student;
import com.example.asv.student.StudentDTO;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class SchoolClassMapper {

    // SchoolClass -> SchoolClassDTO
    public SchoolClassDTO mapToSchoolClassDTO(SchoolClass schoolClass) {
        SchoolClassDTO schoolClassDTO = new SchoolClassDTO();
        schoolClassDTO.setId(schoolClass.getId());
        schoolClassDTO.setIdParallel(schoolClass.getIdParallel());
        schoolClassDTO.setIdTeacher(schoolClass.getIdTeacher());
        schoolClassDTO.setCurrentYear(schoolClass.getCurrentYear());

        return schoolClassDTO;
    }

    // List<SchoolClassDTO> -> SchoolClassResponseDTO
    public SchoolClassResponseDTO mapToSchoolClassResponseDTO(List<SchoolClassDTO> schoolClassDTOList, Integer currentPage, Integer totalItems) {
        SchoolClassResponseDTO schoolClassResponseDTO = new SchoolClassResponseDTO();
        schoolClassResponseDTO.setSchoolClassDTOList(schoolClassDTOList);
        schoolClassResponseDTO.setCurrentPage(currentPage);
        schoolClassResponseDTO.setTotalItems(totalItems);

        return schoolClassResponseDTO;
    }

    // SchoolClassDTO -> SchoolClass
    public SchoolClass mapToSchoolClass(SchoolClassDTO schoolClassDTO) {
        SchoolClass schoolClass = new SchoolClass();
        schoolClass.setId(schoolClassDTO.getId());
        schoolClass.setIdParallel(schoolClassDTO.getIdParallel());
        schoolClass.setIdTeacher(schoolClassDTO.getIdTeacher());
        schoolClassDTO.setCurrentYear(schoolClassDTO.getCurrentYear());
        return schoolClass;
    }
}
