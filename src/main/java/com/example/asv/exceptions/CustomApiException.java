package com.example.asv.exceptions;

import lombok.Data;
import org.springframework.http.HttpStatus;

@Data
// класс для кастомных исключений
public class CustomApiException extends Exception {
    private HttpStatus status;

    // Кастомное исключение, в котором передается ошибка и код ошибки при обращении по API
    public CustomApiException(String message, HttpStatus httpStatus) {
        super(message);
        this.status = httpStatus;
    }
}
