package com.example.asv.student;

import com.example.asv.exceptions.CustomApiException;
import com.example.asv.jooq.Tables;
import com.example.asv.jooq.tables.pojos.SchoolClass;
import com.example.asv.jooq.tables.pojos.Student;
import com.example.asv.schoolclass.SchoolClassDTO;
import com.example.asv.schoolclass.SchoolClassRepository;
import org.apache.log4j.Logger;
import org.jooq.Condition;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.util.Objects;

import static org.jooq.impl.DSL.log;
import static org.jooq.impl.DSL.trueCondition;

@Service
public class StudentService {
    static Logger logger = Logger.getLogger(StudentService.class);

    private final StudentMapper studentMapper = new StudentMapper();
    private final Integer defaultPageSize = 50;
    @Autowired
    public StudentRepository studentRepository;
    @Autowired
    public SchoolClassRepository schoolClassRepository;

    public StudentDTO create (Student student) throws CustomApiException {
        // Если передали id, то возвращаем ошибку
        if (student.getId() != null) {
            logger.error("Id field is prohibited");
            throw new CustomApiException("Id field is prohibited", HttpStatus.BAD_REQUEST);
        }
        // Если не передали fullName, то возвращаем ошибку
        if (student.getFullName() == null) {
            logger.error("FullName can't be empty");
            throw new CustomApiException("FullName can't be empty", HttpStatus.BAD_REQUEST);
        }
        // Если передали пустой fullName, то возвращаем ошибку
        if (Objects.equals("", student.getFullName())) {
            logger.error("FullName can't be empty");
            throw new CustomApiException("FullName can't be empty", HttpStatus.BAD_REQUEST);
        }
        // Если не передали EntityId, то возвращаем ошибку
        if (student.getEntityId() == null) {
            logger.error("EntityId can't be empty");
            throw new CustomApiException("EntityId can't be empty", HttpStatus.BAD_REQUEST);
        }
        // Если не передали IdSchoolClass, то возвращаем ошибку
        if (student.getIdSchoolClass() == null) {
            logger.error("IdSchoolClass can't be empty");
            throw new CustomApiException("IdSchoolClass can't be empty", HttpStatus.BAD_REQUEST);
        }

        return studentRepository.insert(student);
    }

    public StudentDTO getStudent (Integer id) throws CustomApiException {
        StudentDTO studentDTO = studentRepository.find(id);
        // Нет сущности с таким идентификатором
        if (studentDTO == null) {
            logger.error(String.format("Student with id %d not found", id));
            throw new CustomApiException(String.format("Student with id %d not found", id), HttpStatus.NOT_FOUND);
        }
        return studentDTO;
    }

    public StudentResponseDTO getStudents (StudentDTO studentDTO, Integer pageSize, Integer pageNumber) {

        if (pageSize > defaultPageSize) {
            pageSize = defaultPageSize;
        }

        Condition condition = trueCondition();
        if (studentDTO.getEntityId() != null) {
            condition = condition.and(Tables.STUDENT.ENTITY_ID.containsIgnoreCase(studentDTO.getEntityId()));
        }
        if (studentDTO.getFullName() != null) {
            condition = condition.and(Tables.STUDENT.FULL_NAME.containsIgnoreCase(studentDTO.getFullName()));
        }
        if (studentDTO.getIdSchoolClass() != null) {
            condition = condition.and(Tables.STUDENT.ID_SCHOOL_CLASS.containsIgnoreCase(studentDTO.getIdSchoolClass()));
        }

        return studentMapper.mapToStudentResponseDTO(
                studentRepository.findAll(condition, pageSize, pageNumber),
                pageNumber,
                studentRepository.getTotalItems(condition)
        );
    }

    public boolean delete (Integer id) throws CustomApiException {
        StudentDTO studentDTO = studentRepository.find(id);

        // Нет сущности с таким идентификатором
        if (studentDTO == null) {
            logger.error(String.format("Student with id %d not found", id));
            throw new CustomApiException(String.format("Student with id %d not found", id), HttpStatus.NOT_FOUND);
        }

        // ставим дату удаления для ученика
        Student student = studentMapper.mapToStudent(studentDTO);
        student.setDeletionTime(LocalDateTime.now());

        return studentRepository.delete(id, student);
    }

    public StudentDTO update (Integer id, Student student) throws CustomApiException {
        StudentDTO studentDTO = studentRepository.update(id, student);

        // Нет сущности с таким идентификатором
        if (studentDTO == null) {
            logger.error(String.format("Student with id %d not found", id));
            throw new CustomApiException(String.format("Student with id %d not found", id), HttpStatus.NOT_FOUND);
        }

        return studentDTO;
    }

    public StudentDTO transferStudent(Integer studentId, Integer schoolClassId) throws CustomApiException {

        StudentDTO studentDTO = studentRepository.find(studentId);
        SchoolClassDTO schoolClassDTO = schoolClassRepository.find(schoolClassId);

        // Нет сущности с таким идентификатором
        if (schoolClassDTO == null) {
            logger.error(String.format("School class with id %d not found", schoolClassId));
            throw new CustomApiException(String.format("School class with id %d not found", schoolClassId), HttpStatus.NOT_FOUND);
        }
        // Нет сущности с таким идентификатором
        if (studentDTO == null) {
            logger.error(String.format("School class with id %d not found", schoolClassId));
            throw new CustomApiException(String.format("Student with id %d not found", schoolClassId), HttpStatus.NOT_FOUND);
        }

        Student student = studentMapper.mapToStudent(studentDTO);
        // ставим дату удаления для ученика
        student.setDeletionTime(LocalDateTime.now());
        studentRepository.delete(studentId, student);
        // добавляем новую запись об ученике
        student.setId(null);
        student.setIdSchoolClass(schoolClassId);
        student.setDeletionTime(null);

        return studentRepository.insert(student);
    }
}
