package com.example.asv.student.csv;

import com.example.asv.jooq.tables.records.StudentRecord;
import com.example.asv.student.StudentRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.List;

@Service
public class StudentCSVService {
    @Autowired
    StudentRepository studentRepository;

    // сохранить данные в БД из CSV-файла
    public void save(MultipartFile file) {
        try {
            List<StudentRecord> schoolClassRecordList = StudentCSVMapper.csvToStudents(file.getInputStream());
            studentRepository.insertAll(schoolClassRecordList);
        } catch (IOException e) {
            throw new RuntimeException("fail to store csv " + e.getMessage());
        }
    }
}
