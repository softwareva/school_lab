package com.example.asv.student.csv;

import com.example.asv.schoolclass.csv.SchoolClassCSVController;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

@RestController
@RequestMapping(value = "/student/csv")
// контроллер для загрузки учеников из csv файла
public class StudentCSVController {
    static Logger logger = Logger.getLogger(SchoolClassCSVController.class);

    @Autowired
    StudentCSVService csvService;

    @PostMapping("/upload")
    public ResponseEntity<StudentCSVResponseDTO> uploadFile(@RequestParam("file") MultipartFile file) {
        String message = "";

        // проверяем на формат CSV файл
        if (StudentCSVMapper.hasCSVFormat(file)) {
            try {
                csvService.save(file);

                message = "Uploaded the file successfully: " + file.getOriginalFilename();
                StudentCSVResponseDTO StudentCSVResponseDTO = new StudentCSVResponseDTO();
                StudentCSVResponseDTO.setMessage(message);

                logger.info(message);

                return ResponseEntity.ok(StudentCSVResponseDTO);
            } catch (Exception e) {

                message = "Could not upload the file: " + file.getOriginalFilename() + "!";

                StudentCSVResponseDTO StudentCSVResponseDTO = new StudentCSVResponseDTO();
                StudentCSVResponseDTO.setMessage(message);
                e.printStackTrace();
                logger.error(message);

                return ResponseEntity.status(HttpStatus.EXPECTATION_FAILED).body(StudentCSVResponseDTO);
            }
        }

        message = "Please upload a csv file!";
        StudentCSVResponseDTO StudentCSVResponseDTO = new StudentCSVResponseDTO();
        StudentCSVResponseDTO.setMessage(message);
        logger.error(message);

        return ResponseEntity.badRequest().body(StudentCSVResponseDTO);
    }

}
