package com.example.asv.student.csv;

import lombok.Data;

@Data
public class StudentCSVResponseDTO {
    private String message;
}
