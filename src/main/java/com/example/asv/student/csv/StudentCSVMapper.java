package com.example.asv.student.csv;

import com.example.asv.jooq.tables.records.StudentRecord;
import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVParser;
import org.apache.commons.csv.CSVRecord;
import org.springframework.web.multipart.MultipartFile;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.List;

public class StudentCSVMapper {
    public static String TYPE = "text/csv";
    static String[] HEADERs = { "entity_id", "full_name", "id_school_class" };

    // метод для проверки файла на CSV формат
    public static boolean hasCSVFormat(MultipartFile file) {
        if (!TYPE.equals(file.getContentType())) {
            return false;
        }
        return true;
    }

    // метод для перевода из csv в список школьных классов
    public static List<StudentRecord> csvToStudents(InputStream is) {
        try (BufferedReader fileReader = new BufferedReader(new InputStreamReader(is, StandardCharsets.UTF_8));
             CSVParser csvParser = new CSVParser(fileReader,
                     CSVFormat.DEFAULT.builder().setHeader().setSkipHeaderRecord(true).setIgnoreHeaderCase(true).setTrim(true)
                             .setDelimiter(';')
                             .setRecordSeparator("\r\n").build())) {

            List<StudentRecord> studentRecordList = new ArrayList<StudentRecord>();

            Iterable<CSVRecord> csvRecords = csvParser.getRecords();

            for (CSVRecord csvRecord : csvRecords) {
                StudentRecord studentRecord = new StudentRecord();
                studentRecord.setEntityId(Integer.parseInt(csvRecord.get("entity_id")));
                studentRecord.setFullName(csvRecord.get("full_name"));
                studentRecord.setIdSchoolClass(Integer.parseInt(csvRecord.get("id_school_class")));

                studentRecordList.add(studentRecord);
            }

            return studentRecordList;
        } catch (IOException e) {
            throw new RuntimeException("fail to parse CSV file: " + e.getMessage());
        }
    }
}
