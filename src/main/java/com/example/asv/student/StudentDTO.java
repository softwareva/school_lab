package com.example.asv.student;

import lombok.Data;

import java.sql.Timestamp;

@Data
public class StudentDTO {
    private Integer id;
    private Integer entityId;
    private String fullName;
    private Integer idSchoolClass;
    private Timestamp deletionTime;
}
