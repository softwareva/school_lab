package com.example.asv.student;

import com.example.asv.exceptions.CustomApiException;
import com.example.asv.jooq.tables.pojos.Student;
import com.example.asv.schoolclass.SchoolClassController;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping(value = "/student")
public class StudentController {
    static Logger logger = Logger.getLogger(StudentController.class);

    @Autowired
    private StudentService studentService;

    @GetMapping("/")
    public ResponseEntity<StudentResponseDTO> getStudents(@RequestParam(required = false) Integer entityId,
                                                              @RequestParam(required = false) String fullName,
                                                              @RequestParam(required = false) Integer idSchoolClass,
                                                              @RequestParam(required = false, defaultValue = "50") Integer pageSize,
                                                              @RequestParam(required = false, defaultValue = "0") Integer pageNumber) {
        StudentDTO studentDTO = new StudentDTO();
        studentDTO.setEntityId(entityId);
        studentDTO.setFullName(fullName);
        studentDTO.setIdSchoolClass(idSchoolClass);

        StudentResponseDTO studentResponseDTO = studentService.getStudents(studentDTO, pageSize, pageNumber);
        logger.info("Student list sent successfully");
        return ResponseEntity.ok(studentResponseDTO);
    }

    @GetMapping("/{studentId}")
    public ResponseEntity<StudentDTO> getStudent(@PathVariable(value = "studentId") Integer id) throws CustomApiException {
        StudentDTO studentDTO = studentService.getStudent(id);
        logger.info("Student sent successfully");
        return ResponseEntity.ok(studentDTO);
    }

    @PostMapping("/")
    public ResponseEntity<StudentDTO> createStudent(@RequestBody Student student) throws CustomApiException {
        StudentDTO studentDTO = studentService.create(student);
        logger.info("Student created successfully");
        return ResponseEntity.ok(studentDTO);
    }

    @PutMapping("/{studentId}")
    public ResponseEntity<StudentDTO> updateStudent(@PathVariable(value = "studentId") Integer id,
                                                    @RequestBody Student student) throws CustomApiException {
        StudentDTO studentDTO = studentService.update(id, student);
        logger.info("Student updated successfully");
        return ResponseEntity.ok(studentDTO);
    }

    @DeleteMapping("/{studentId}")
    public ResponseEntity<Object> deleteStudent(@PathVariable(value = "studentId") Integer id) throws CustomApiException {
        boolean t = studentService.delete(id);
        if (t) {
            logger.info("Student deleted successfully");
            return ResponseEntity.ok("Student deleted");
        }
        else {
            logger.error("Error while deleting student");
            return ResponseEntity.badRequest().body("Error");
        }
    }

    @PutMapping(value = "/transfer", params = {"studentId", "schoolClassId"})
    public ResponseEntity<StudentDTO> transferStudent(@RequestParam(value = "studentId") Integer studentId,
                                                      @RequestParam(value = "schoolClassId") Integer schoolClassId) throws CustomApiException {
        StudentDTO studentDTO = studentService.transferStudent(studentId, schoolClassId);
        logger.info("Student transfer successfully");
        return ResponseEntity.ok(studentDTO);
    }
}
