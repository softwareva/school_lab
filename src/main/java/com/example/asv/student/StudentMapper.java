package com.example.asv.student;

import com.example.asv.jooq.tables.pojos.Student;
import org.springframework.stereotype.Service;

import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.util.List;

@Service
public class StudentMapper {

    // Student -> StudentDTO
    public StudentDTO mapToStudentDTO(Student student) {
        StudentDTO studentDTO = new StudentDTO();
        studentDTO.setId(student.getId());
        studentDTO.setEntityId(student.getEntityId());
        studentDTO.setFullName(student.getFullName());
        studentDTO.setIdSchoolClass(student.getIdSchoolClass());
        studentDTO.setDeletionTime(Timestamp.valueOf(student.getDeletionTime()));

        return studentDTO;
    }

    // List<StudentDTO> -> StudentResponseDTO
    public StudentResponseDTO mapToStudentResponseDTO(List<StudentDTO> studentDTOList, Integer currentPage, Integer totalItems) {
        StudentResponseDTO studentResponseDTO = new StudentResponseDTO();
        studentResponseDTO.setStudentDTOList(studentDTOList);
        studentResponseDTO.setCurrentPage(currentPage);
        studentResponseDTO.setTotalItems(totalItems);

        return studentResponseDTO;
    }

    // StudentDTO -> Student
    public Student mapToStudent(StudentDTO studentDTO) {
        Student student = new Student();
        student.setId(studentDTO.getId());
        student.setEntityId(studentDTO.getEntityId());
        student.setFullName(studentDTO.getFullName());
        student.setIdSchoolClass(studentDTO.getIdSchoolClass());
        if (studentDTO.getDeletionTime() == null) {
            student.setDeletionTime(null);
        } else {
            student.setDeletionTime(studentDTO.getDeletionTime().toLocalDateTime());
        }
        return student;
    }
}
