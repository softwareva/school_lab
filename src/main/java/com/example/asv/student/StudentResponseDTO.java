package com.example.asv.student;

import lombok.Data;

import java.util.List;

@Data
public class StudentResponseDTO {
    private Integer totalItems;
    private Integer currentPage;
    private List<StudentDTO> studentDTOList;
}
