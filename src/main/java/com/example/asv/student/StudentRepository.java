package com.example.asv.student;

import com.example.asv.jooq.Tables;
import com.example.asv.jooq.tables.pojos.Student;
import com.example.asv.jooq.tables.records.StudentRecord;
import lombok.RequiredArgsConstructor;
import org.jooq.Condition;
import org.jooq.DSLContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
@RequiredArgsConstructor
public class StudentRepository {
    @Autowired
    private final DSLContext dslContext;

    public StudentDTO find (Integer id) {
        return dslContext.selectFrom(Tables.STUDENT)
                .where(Tables.STUDENT.ENTITY_ID.eq(id).and(Tables.STUDENT.DELETION_TIME.isNull()))
                .fetchOneInto(StudentDTO.class);
    }

    public List<StudentDTO> findAll (Condition condition, Integer pageSize, Integer pageNumber) {
        return dslContext.selectFrom(Tables.STUDENT)
                .where(condition.and(Tables.STUDENT.DELETION_TIME.isNull()))
                .orderBy(Tables.STUDENT.ENTITY_ID)
                .limit(pageSize)
                .offset(pageSize * pageNumber)
                .fetchInto(StudentDTO.class);
    }

    public StudentDTO insert(Student student) {
        return dslContext.insertInto(Tables.STUDENT)
                .set(dslContext.newRecord(Tables.STUDENT, student))
                .returning()
                .fetchOne().into(StudentDTO.class);
    }

    public int[] insertAll(List<StudentRecord> studentRecordList) {
        return dslContext.batchInsert(studentRecordList).execute();
    }

    public StudentDTO update (Integer id, Student student) {
        return dslContext.update(Tables.STUDENT)
                .set(dslContext.newRecord(Tables.STUDENT, student))
                .where(Tables.STUDENT.ENTITY_ID.eq(id).and(Tables.STUDENT.DELETION_TIME.isNull()))
                .returning()
                .fetchOne().into(StudentDTO.class);
    }

    public boolean delete (Integer id, Student student) {
        return dslContext.update(Tables.STUDENT)
                .set(dslContext.newRecord(Tables.STUDENT, student))
                .where(Tables.STUDENT.ENTITY_ID.eq(id).and(Tables.STUDENT.DELETION_TIME.isNull()))
                .execute() == 1;
    }

    public List<StudentDTO> findStudentsBySchoolClassId (Integer id) {
        return dslContext.selectFrom(Tables.STUDENT)
                .where(Tables.STUDENT.ID_SCHOOL_CLASS.eq(id).and(Tables.STUDENT.DELETION_TIME.isNull()))
                .fetchInto(StudentDTO.class);
    }

    public Integer getTotalItems(Condition condition) {
        return dslContext.selectCount()
                .from(Tables.STUDENT)
                .where(condition)
                .fetchOneInto(Integer.class);
    }
}
