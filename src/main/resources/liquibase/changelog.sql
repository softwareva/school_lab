--changeset liquibase:1
CREATE TABLE parallel (
    id SERIAL NOT NULL PRIMARY KEY,
    parallel INTEGER NOT NULL UNIQUE
);

--changeset liquibase:2
CREATE TABLE grade (
    id SERIAL NOT NULL PRIMARY KEY,
    grade VARCHAR(20) NOT NULL UNIQUE
);

--changeset liquibase:3
CREATE TABLE teacher (
    id SERIAL NOT NULL PRIMARY KEY,
    full_name VARCHAR(100) NOT NULL
);

--changeset liquibase:4
CREATE TABLE school_class (
    id SERIAL NOT NULL PRIMARY KEY,
    id_parallel INTEGER NOT NULL REFERENCES parallel(id),
    id_teacher INTEGER NOT NULL REFERENCES teacher(id),
    current_year INTEGER NOT NULL
);

--changeset liquibase:5
CREATE TABLE student (
    id SERIAL NOT NULL PRIMARY KEY,
    entity_id INTEGER NOT NULL,
    full_name VARCHAR(100) NOT NULL,
    id_school_class INTEGER NOT NULL REFERENCES school_class(id),
    deletion_time TIMESTAMP
);

--changeset liquibase:6
insert into parallel (id, parallel)
       values (1, 1),(2, 2),(3, 3),(4, 4),
              (5, 5),(6, 6),(7, 7),(8, 8),
              (9, 9),(10, 10),(11, 11);

--changeset liquibase:7
insert into grade (id, grade)
       values (1, 'Начальная школа'), (2, 'Средняя школа'), (3, 'Старшая школа');
