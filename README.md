# School Lab

## Требования к сборке

1. JDK 11
2. Maven 3.6.3
3. Docker

## Запуск БД и веб-приложения

Запуск БД
```
docker-compose up -d
```
Остановить контейнер с БД
```
docker-compose stop
```
Удалить контейнер с БД
```
docker-compose down
```

После собираем и запускаем веб-приложение
```
docker build -t "web-spring" --network host .
docker run --network host -d web-spring
```
Остановить контейнер
```
docker stop <container id>
```

## Документация (swagger)

http://localhost:8080/swagger-ui/index.html

## Логирование

log/log_file.log

## Работа с CSV-файлами

Имеется возможность загрузить данные через csv файл для сущностей SchoolClass и Student.

Разделитель - точка с запятой (;)

Заголовок в запросе "file"

### Формат ввода для SchoolClass:

"id_parallel", "id_teacher", "current_year"

Пример:

id_parallel;id_teacher;current_year

1;1;2023

### Формат ввода для Student:

"entity_id", "full_name", "id_school_class"

Пример:

entity_id;full_name;id_school_class

1;"Иванов Иван Иванович";1
