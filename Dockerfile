FROM maven:3.6.3-openjdk-11 AS build
COPY src /usr/src/app/src
COPY pom.xml /usr/src/app
RUN mvn -f /usr/src/app/pom.xml liquibase:update
RUN mvn -f /usr/src/app/pom.xml jooq-codegen:generate
RUN mvn -f /usr/src/app/pom.xml package

FROM openjdk:11.0.16-jre 
COPY --from=build /usr/src/app/target/lab-0.0.1-SNAPSHOT.jar /usr/app/lab-0.0.1-SNAPSHOT.jar
ENTRYPOINT ["java", "-jar", "/usr/app/lab-0.0.1-SNAPSHOT.jar"]
